#!/usr/bin/env python3
import ppb
from ppb_mutant import MutantSprite
from hugs import MotionMixin

class DemoSprite(MotionMixin, MutantSprite):
    emoji = 'travel_places/transport/aerial_tramway'
    max_speed = 1.5

    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)
        self.position = ppb.Vector(-2, 0)
        self.target = ppb.Vector(+2, 0)

    def on_update(self, update, signal):
        op = self.position
        super().on_update(update, signal)
        np = self.position

        print(
            update.time_delta,
            (np - op),
            ((np - op) * (1 / update.time_delta)).length
        )

class DemoScene(ppb.BaseScene):
    def __init__(self, *p, **kw):
        super().__init__(*p, background_color=(0, 100, 0), **kw)

        self.add(DemoSprite())


if __name__ == '__main__':
    ppb.run(starting_scene=DemoScene)

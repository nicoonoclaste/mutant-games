#!/usr/bin/env python3
"""
Show a grid of emoji, aligned.
"""
import ppb
from ppb_mutant import MutantSprite
from utils import (
    CircularRegion, MenuScene
)

class FaceSprite(MutantSprite, CircularRegion):
    normal_face = 'expressions/smileys/happy_fun/smile'
    hover_face = 'expressions/smileys/happy_fun/big_smile'

    emoji = normal_face

    def on_mouse_motion(self, mouse, signal):
        if self.contains(mouse.position):
            self.emoji = self.hover_face
        else:
            self.emoji = self.normal_face

    def on_button_pressed(self, mouse, signal):
        if self.contains(mouse.position):
            mouse.scene.main_camera.position = self.position


class OriginFaceSprite(FaceSprite):
    normal_face = 'expressions/smileys/cats/cat_smile'
    hover_face = 'expressions/smileys/cats/cat_grin'

    emoji = normal_face


class GridScene(MenuScene):
    def __init__(self, *p, **kw):
        super().__init__(*p, pixel_ratio=64, **kw)

    def get_options(self):
        cam = self.main_camera
        print(f"({cam.frame_left}, {cam.frame_top}) -> ({cam.frame_right}, {cam.frame_bottom})")
        for x in range(int(cam.frame_left) - 1, int(cam.frame_right) + 1):
            for y in range(int(cam.frame_top) - 1, int(cam.frame_bottom) + 1):
                if x == 0 and y == 0:
                    yield OriginFaceSprite(pos=(x, y))
                else:
                    yield FaceSprite(pos=(x, y))


if __name__ == '__main__':
    ppb.run(
        starting_scene=GridScene,
        # resolution=(700, 700),
        # window_title='Grid Test',
    )

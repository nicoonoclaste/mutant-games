#!/usr/bin/env python3
"""
Select your monster by clicking on it.

Click and hold the mouse to move the monster.

Try to hug all the people by getting close to them.
"""
import ppb
import math
import time
import random
from ppb_mutant import MutantSprite
from utils import (
    CircularRegion, CircularMenuScene, AnimationSprite, MenuSprite,
    clamp,
)


print(__doc__)


class MotionMixin:
    target: ppb.Vector = None
    velocity: ppb.Vector = None
    max_speed: float = None

    def on_update(self, update, signal):
        if self.target is not None:
            if self.max_speed is None:
                self.position = self.target
            else:
                delta = (self.target - self.position)
                # Calculate the maximum amount we could travel in this time, and
                # limit to that.
                delta = delta.truncate(self.max_speed * update.time_delta)
                self.position += delta
        elif self.velocity is not None:
            v = self.velocity
            if self.max_speed is not None:
                v = v.truncate(self.max_speed)
            self.position += v * update.time_delta

        camera = update.scene.main_camera
        self.position = ppb.Vector(clamp(camera.frame_left, self.position.x, camera.frame_right),
                                   clamp(camera.frame_top, self.position.y, camera.frame_bottom),
        )

def sign(x):
    from math import copysign
    return copysign(1, x)

class RunnerSprite(MotionMixin, MutantSprite, CircularRegion):
    emoji = 'expressions/smileys/shock_fear_exhaustion/scared'

    max_speed = 1.0

    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)
        self.velocity = ppb.Vector(0, 1).rotate(
            random.uniform(-180, 180)
        )

    def _check_walls(self, camera):
        # TODO: Do something gentler
        if not (camera.frame_left < self.position.x < camera.frame_right):
            if sign(camera.position.x - self.position.x) != sign(self.velocity.x):
                self.velocity = self.velocity.reflect(ppb.Vector(1, 0))

        if not (camera.frame_top < self.position.y < camera.frame_bottom):
            if sign(camera.position.y - self.position.y) != sign(self.velocity.y):
                self.velocity = self.velocity.reflect(ppb.Vector(0, 1))

    def on_update(self, update, signal):
        rot = random.triangular(-10, +10)
        self.velocity = self.velocity.rotate(rot)

        self._check_walls(update.scene.main_camera)

        super().on_update(update, signal)


class HuggedSprite(MutantSprite, CircularRegion):
    emoji = 'expressions/smileys/embarrassed_affection/smile_hearts'


class HeartAnimSprite(MutantSprite, AnimationSprite):
    emoji = 'symbols/hearts/red_heart'

    line = ppb.Vector(0, -1)
    duration = 1

    def do_start(self, signal):
        self.position = self.aposition

    def do_frame(self, dt, t, signal):
        self.position = self.line * (t / self.duration) + self.aposition
        return t < self.duration


class PlayerSprite(MotionMixin, MutantSprite, CircularRegion):
    max_speed = 1.05

    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)
        self.target = None
        self.velocity = ppb.Vector(0, 0)

    def on_button_pressed(self, mouse, signal):
        if mouse.button is ppb.buttons.Primary:
            self.target = mouse.position

    def on_button_released(self, mouse, signal):
        if mouse.button is ppb.buttons.Primary:
            self.target = None

    def on_mouse_motion(self, mouse, signal):
        if ppb.buttons.Primary in mouse.buttons:
            self.target = mouse.position


class MainScene(ppb.BaseScene):
    runner_count = 10

    def __init__(self, *p, **kw):
        super().__init__(*p, background_color=(0, 100, 0), **kw)

        for _ in range(self.runner_count):
            self.add(RunnerSprite(pos=(
                random.uniform(self.main_camera.frame_left, self.main_camera.frame_right),
                random.uniform(self.main_camera.frame_top, self.main_camera.frame_bottom),
            )))
        self.add(PlayerSprite())

    def hug(self, runner):
        hugged = HuggedSprite(pos=runner.position)
        self.remove(runner)
        self.add(hugged)
        self.add(HeartAnimSprite(anchor=hugged))

    def on_update(self, update, signal):
        bear = next(self.get(kind=PlayerSprite))
        count = 0
        for runner in self.get(kind=RunnerSprite):
            count += 1
            if bear.contains(runner):
                self.hug(runner)

        if not count:
            signal(ppb.events.Quit())


class CharacterSelectSprite(MutantSprite, CircularRegion, MenuSprite):
    def __init__(self, *p, emoji, **kw):
        super().__init__(*p, **kw)
        self.emoji = emoji


class CharacterSelectScene(CircularMenuScene):
    ring_increment = 1.25
    item_size = 1.25

    characters = [
        'puffer_fish',
        'owl',
        'coyote',
        'fox',
        'hyena',
        'jackal',
        'wolf',
        'troll',
        'oni',
        'goblin',
        'dark_elf',
        'minotaur',
        'orc',
        'bugbear',
        'demon',
        'elf',
        'kobold',
        'half_demon',
        'fish_person',
        'tiger',
        'cheetah',
        'lion_with_mane',
        'lion_without_mane',
        'jaguar',
        'leopard',
        'lynx',
        'snow_leopard',
        'slime',
        'deer_without_antlers',
        'bear',
        'raccoon',
        'mouse',
        'ram',
        'opossum',
        'rat',
        'rabbit',
        'red_panda',
        'panda',
        'deer_with_antlers',
        'otter',
        'snake',
        'frankensteins_monster',
        'clown',
        'tengu_mask',
        'robot',
        'alien_monster',
        'alien',
    ]

    def get_options(self):
        for e in self.characters:
            yield CharacterSelectSprite(emoji=e)

    def do_select(self, sprite, signal):
            # FIXME: Better way to send this data?
            PlayerSprite.emoji = sprite.emoji
            self.next = MainScene
            self.running = False


if __name__ == '__main__':
    ppb.run(
        starting_scene=CharacterSelectScene,
        # resolution=(700, 700),
        # window_title='Hug the Humans!',
    )
